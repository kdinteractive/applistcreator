package cool.ben.kurio;

public class Apk {

    private String file;
    private String icon;
    private String label, labelFR, labelNL, labelDE, labelES;
    private String packageName;
    private boolean selected;
    private boolean frSelected;
    private boolean deSelected;
    private boolean nlSelected;
    private boolean esSelected;
    private String type;
    private String versionCode;
    private String priority;

    public Apk(){
    }

    public Apk(String file, String icon, String label, String packageName,boolean selected, String type, String versionCode,String priority, String labelFR, String labelNL, String labelDE, String labelES){
        this.file = file;
        this.icon = icon;
        this.label = label;
        this.labelNL = labelNL;
        this.labelFR = labelFR;
        this.labelDE = labelDE;
        this.labelES = labelES;
        this.packageName = packageName;
        this.selected = selected;
        this.type = type;
        this.versionCode = versionCode;
        this.priority = priority;
    }

    public Apk(String file, String icon, String label, String packageName,boolean selected, String type, String versionCode,String priority){
        this.file = file;
        this.icon = icon;
        this.label = label;
        this.packageName = packageName;
        this.selected = selected;
        this.type = type;
        this.versionCode = versionCode;
        this.priority = priority;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getLabelFR() {
        return labelFR;
    }

    public void setLabelFR(String labelFR) {
        this.labelFR = labelFR;
    }

    public String getLabelNL() {
        return labelNL;
    }

    public void setLabelNL(String labelNL) {
        this.labelNL = labelNL;
    }

    public String getLabelDE() {
        return labelDE;
    }

    public void setLabelDE(String labelDE) {
        this.labelDE = labelDE;
    }

    public String getLabelES() { return labelES; }

    public void setLabelES(String labelES) { this.labelES = labelES; }

    public boolean isFrSelected() {
        return frSelected;
    }

    public void setFrSelected(boolean frSelected) {
        this.frSelected = frSelected;
    }

    public boolean isDeSelected() {
        return deSelected;
    }

    public void setDeSelected(boolean deSelected) {
        this.deSelected = deSelected;
    }

    public boolean isNlSelected() {
        return nlSelected;
    }

    public void setNlSelected(boolean nlSelected) {
        this.nlSelected = nlSelected;
    }

    public boolean isEsSelected() { return esSelected; }

    public void setEsSelected(boolean esSelected) { this.esSelected = esSelected; }
}
