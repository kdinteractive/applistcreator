package cool.ben.kurio;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

    public static final int ROW_FOR_TYPE = 5;
    static int columnWithCross = 0;
    static int columnWithFile = 0;
    static int columnWithIcon = 0;
    static int columnWithLabel = 0;
    static int columnWithLabelFr = 0;
    static int columnWithLabelNl = 0;
    static int columnWithLabelDe = 0;
    static int columnWithLabelEs = 0;
    static int columnWithPackageName = 0;
    static int columnWithType = 0;
    static int columnWithVersionCode = 0;
    static String fileToGenerate, destDir, path, operatingSystem, tabletModel, languageForLabels, tabletSize, brandName, androidVersion, updateRequested, errorsToDisplay, appFileTwoFirstIntVersionNumber;
    static File fileToMine;
    static boolean multiLanguage = false;
    static ArrayList<String> applistsLanguages = new ArrayList<>();
    private static String type;
    public static void main(String[] args) throws Exception {
        //Mettre le fichier xlsx au chemin indiqué ci-dessous fileToMine
        operatingSystem = System.getProperty("os.name").toLowerCase();
        //Windows
        if (operatingSystem.contains("windows")) {
            fileToGenerate = "\\applist.xml";
            //Chemins sur la machine de Thomas
            destDir = "D:\\apkpack\\GENERATED_APPLIST\\";
            fileToMine = new File("D:\\apkpack\\GENERATED_APPLIST\\APKPreloadListByCountry.xlsx");
            System.out.println("OS: Windows");
        }
        //Ubuntu
        else if (operatingSystem.contains("linux")) {
            fileToGenerate = "/applist.xml";
            //Chemins sur la machine de Thomas
            destDir = "/home/arronis/Documents/TEST/GENERATED_APPLIST/";
            fileToMine = new File("/home/arronis/Documents/TEST/GENERATED_APPLIST/APKPreloadListByCountry.xlsx");
            System.out.println("OS: Linux");
        } else {
            System.out.println("Impossible de détecter l'OS");
            return;
        }
        XSSFWorkbook myWorkBook = null;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Le fichier xlsx doit être dans : \n" + fileToMine + "\n");
        System.out.print("ROM de la tablette : (Ex: UX0) ");
        tabletModel = scanner.next().toUpperCase();

        System.out.print("Est-ce une update? (o/n)");
        updateRequested = scanner.next().toUpperCase();

        System.out.print("Version du fichier applist.xml : ");
        int applistVersion = Integer.parseInt(scanner.next().toUpperCase());

        System.out.print("Numéro de page concerné (à partir de la 3 (2021) et +) : ");
        int pageToMine = Integer.parseInt(scanner.next());

        System.out.print("La taille de la tablette : (Ex: 7 pour 7 pouces) ");
        tabletSize = scanner.next();

        System.out.print("La version Android : (Ex: 10 pour android 10) ");
        androidVersion = scanner.next();

        System.out.print("Les deux premiers chiffres de la version générée indiquées sur l'apk : (Ex: 10 pour kuriogenius_10.34.apk) ");
        appFileTwoFirstIntVersionNumber = scanner.next();

        System.out.print("Nom du brandlogo et wallpaper: (Ex: gulli, nick, toggo, studio (studio 100) ou rien (littéralement)) ");
        brandName = scanner.next().toLowerCase();

        path = destDir + tabletModel;
        System.out.print("Chemin absolu du dossier de destination: \n" + path + "\n");
        File folder = new File(path);
        folder.mkdir();


        try {
            FileInputStream fis = new FileInputStream(fileToMine);
            myWorkBook = new XSSFWorkbook(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String preloadFolderName = path + (operatingSystem.contains("windows") ? "\\" : "/") + "preload" + (operatingSystem.contains("windows") ? "\\" : "/");

        //syscheck file
        FileWriter sysCheck = new FileWriter(path + (operatingSystem.contains("windows") ? "\\" : "/") + "sys.check");
        sysCheck.write("[Kurio System Update]\nSN=1234567890+001\nVersion=1.23");
        sysCheck.close();

        File preloadFolder = new File(preloadFolderName);
        if (preloadFolder.exists()) {
            preloadFolder.delete();
        }
        preloadFolder.mkdir();

        String urlAssets = "https://www.kdtablets.com/downloads/tablets_assets/";
        String urlLastKurioVersions = "https://www.kdtablets.com/downloads/oreoPackage/Android" + androidVersion + "/";
        String[] avatarFiles = new String[]{"neutral_avatar.png", "boy_avatar.png", "girl_avatar.png"};
        String[] imageFiles = new String[]{"google_assistant.icon", "banner_gulli.png", "icon_wp.png", "icon_genius.png", "icon_avatar.png"};
        String[] xmlFiles = new String[]{"toolbar.xml"};
        String[] jsonFiles = new String[]{"customisator.json"};
        String[] wallpaper = new String[]{"neutral_wallpaper.png"};
        String[] kgklVersions = new String[]{"kuriogenius_", "kuriolauncher_"};
        ArrayList<String> apkAvatarWallpaperVersions = new ArrayList<>();

        //Download Kurio Genius and Launcher latest versions
        for (String kurioVersionToDownload : kgklVersions) {
            downloadLastVersion(kurioVersionToDownload,urlLastKurioVersions,preloadFolderName);
        }

        //Download correct avatar and wallpaper apks
        if (tabletSize.equals("10")) {
            urlLastKurioVersions += "10inches/";
            apkAvatarWallpaperVersions.add("Kurio_Wallpaper_");
            apkAvatarWallpaperVersions.add("GO_avatar_");
        } else if (tabletSize.equals("7")) {
            urlLastKurioVersions += "7inches/";
            apkAvatarWallpaperVersions.add("Avatar_From_TAB3_");
            if(brandName.equalsIgnoreCase("gulli")) {
                apkAvatarWallpaperVersions.add("Gulli_Wallpaper_");
            } else {
                apkAvatarWallpaperVersions.add("Kurio_Wallpaper_");
            }
        }

        for (String apkVersionToDownload : apkAvatarWallpaperVersions) {
            if(apkVersionToDownload.startsWith("Gulli")) {
                downloadLastVersion(apkVersionToDownload,urlLastKurioVersions+"Gulli/",preloadFolderName);
            } else {
                downloadLastVersion(apkVersionToDownload,urlLastKurioVersions,preloadFolderName);
            }
        }

        String[][] filesForPreload = new String[][]{avatarFiles, imageFiles, xmlFiles, jsonFiles, wallpaper};
        for (String[] filesTable : filesForPreload) {
            for (String fileToDownload : filesTable) {
                if (fileToDownload.contains("avatar") && !fileToDownload.contains("icon")) {
                    if (downloadFromServer(fileToDownload, urlAssets + "avatars/" + tabletSize + "/", preloadFolderName, "icon")) {
                        System.out.println("Avatar téléchargé : " + fileToDownload);
                    }
                } else if (fileToDownload.contains("wallpaper")) {
                    if (brandName != null) {
                        if (!downloadFromServer(fileToDownload, urlAssets + "wallpapers/" + tabletSize + "/" + brandName + "/", preloadFolderName, "icon")) {
                            if (downloadFromServer(fileToDownload, urlAssets + "wallpapers/" + tabletSize + "/", preloadFolderName, "icon")) {
                                System.out.println("Wallpapar téléchargé : " + fileToDownload);
                            } else {
                                System.out.println("Vérifier (sauf si déjà téléchargé) si le fond d'écran est bien sur le serveur, à savoir: " + fileToDownload);
                            }
                        } else {
                            System.out.println("Wallpapar " + brandName + "  téléchargé : " + fileToDownload);
                        }

                    }
                } else {
                    if (downloadFromServer(fileToDownload, urlAssets, preloadFolderName, "icon")) {
                        System.out.println("Image téléchargé : " + fileToDownload);
                    }
                }

            }
        }

        if(brandName != null && !brandName.equals("rien")) {
            if(downloadFromServer(brandName +".png", urlAssets + "brand_logo/", preloadFolderName, "brand_logo")) {
                System.out.println("Brand logo téléchargé : " + brandName);
            }
        }
        ArrayList<Apk> arrayListApps = appListExtractor(myWorkBook.getSheetAt(pageToMine), tabletModel);
        if(multiLanguage) {
            applistsLanguages.remove("en");
            for (String language : applistsLanguages) {
                appListCreator(applistVersion, "applist_" + language + ".xml", arrayListApps, preloadFolderName, language);
            }
        }
        appListCreator(applistVersion, fileToGenerate, arrayListApps, preloadFolderName, "default");
        if(errorsToDisplay != null) System.out.println(errorsToDisplay);
    }

    public static ArrayList<Apk> appListExtractor(XSSFSheet mySheet, String tabletModel) {
        ArrayList<Apk> listExtractedApk = new ArrayList<>();
        final String fileColumn = "file", iconColumn = "icon", packageNameColumn = "packagename", typeColumn = "category", versionCodeColumn = "versioncode", labelColumn = "label", labelColumnNL = "label_nl", labelColumnDE = "label_de", labelColumnFR = "label_fr", labelColumnES = "label_es";
        Scanner scanner = new Scanner(System.in);

        //Ligne où il y a file, packageName etc
        XSSFRow rowModelsAndROM = mySheet.getRow(ROW_FOR_TYPE);
        Iterator<Cell> cellIteratorModelRow = rowModelsAndROM.cellIterator();

        String appFolderName = path + (operatingSystem.contains("windows") ? "\\" : "/") + "app" + (operatingSystem.contains("windows") ? "\\" : "/");
        File appFolder = new File(appFolderName);
        if (!appFolder.exists()) {
            appFolder.mkdir();
        }

        switch ((tabletModel.charAt(0) + "").toUpperCase()) {
            case "F":
                languageForLabels = "fr";
                break;
            case "D":
                languageForLabels = "de";
                break;
            case "N":
                languageForLabels = "nl";
                break;
            default:
                languageForLabels = "en";
        }

        System.out.print("Le ROM ID est "+ tabletModel +" le langage par défaut devrait donc être " + languageForLabels.toUpperCase() + "? (o/n)");
        if(scanner.next().toLowerCase().equals("n")){
            System.out.print("Quelle langue par défaut ? (fr/de/nl/en/es) \n");
            languageForLabels = scanner.next().toLowerCase();
        }

        //looking for columns
        while (cellIteratorModelRow.hasNext()) {
            Cell cell = cellIteratorModelRow.next();
            if(updateRequested.equalsIgnoreCase("o")) {
                updateRequested = "update";
            } else if (updateRequested.equalsIgnoreCase("n")) {
                updateRequested = "";
            }
            if (cell.getStringCellValue().contains(tabletModel) && cell.getStringCellValue().toLowerCase().contains(updateRequested)) {
                columnWithCross = cell.getColumnIndex();
                if (cell.getStringCellValue().contains("multi")) {
                    multiLanguage = true;
                    String multi = cell.getStringCellValue().substring(cell.getStringCellValue().lastIndexOf("multi")).split("_")[1];
                    applistsLanguages.addAll(Arrays.asList(multi.split("-")));
                }
            }
            switch (cell.getStringCellValue().toLowerCase()) {
                case fileColumn:
                    columnWithFile = cell.getColumnIndex();
                    break;
                case iconColumn:
                    columnWithIcon = cell.getColumnIndex();
                    break;
                case labelColumn:
                    columnWithLabel = cell.getColumnIndex();
                    break;
                case labelColumnFR:
                    columnWithLabelFr = cell.getColumnIndex();
                    break;
                case labelColumnNL:
                    columnWithLabelNl = cell.getColumnIndex();
                    break;
                case labelColumnDE:
                    columnWithLabelDe = cell.getColumnIndex();
                    break;
                case labelColumnES:
                    columnWithLabelEs = cell.getColumnIndex();
                    break;
                case packageNameColumn:
                    columnWithPackageName = cell.getColumnIndex();
                    break;
                case typeColumn:
                    columnWithType = cell.getColumnIndex();
                    break;
                case versionCodeColumn:
                    columnWithVersionCode = cell.getColumnIndex();
                    break;
            }
        }

        //looking for values and select only apk with a cross
        Iterator<Row> rowIterator = mySheet.iterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            //avoid line with file icon etc values
            if (row.getRowNum() == ROW_FOR_TYPE) row = rowIterator.next();
            Apk apk = new Apk();
            // For each row, iterate through each columns
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                int currentColumn = cell.getColumnIndex();
                if (cell.getCellType().equals(CellType.STRING) && cell.getStringCellValue() != null) {
                    String cellValue = cell.getStringCellValue();
                    if (currentColumn == columnWithFile) {
                        apk.setFile(cellValue);
                    } else if (currentColumn == columnWithIcon) {
                        apk.setIcon(cellValue);
                    } else if (currentColumn == columnWithLabel) {
                        if (apk.getLabel() == null) {
                            apk.setLabel(cellValue);
                        }
                    } else if (currentColumn == columnWithLabelNl) {
                        if (languageForLabels.equals("nl")) {
                            apk.setLabel(cellValue);
                        }
                        if (multiLanguage) {
                            apk.setLabelNL(cellValue);
                        }
                    } else if (currentColumn == columnWithLabelFr) {
                        if (languageForLabels.equals("fr")) {
                            apk.setLabel(cellValue);
                        }
                        if (multiLanguage) {
                            apk.setLabelFR(cellValue);
                        }
                    } else if (currentColumn == columnWithLabelDe) {
                        if (languageForLabels.equals("de")) {
                            apk.setLabel(cellValue);
                        }
                        if (multiLanguage) {
                            apk.setLabelDE(cellValue);
                        }
                    } else if (currentColumn == columnWithLabelEs) {
                        if (languageForLabels.equals("es")) {
                            apk.setLabel(cellValue);
                        }
                        if (multiLanguage) {
                            apk.setLabelES(cellValue);
                        }
                    } else if (currentColumn == columnWithPackageName) {
                        apk.setPackageName(cellValue);
                    } else if (currentColumn == columnWithType) {
                        cellValue = cellValue.toLowerCase();
                        if (cellValue.contains("educational")) {
                            cellValue = "educative";
                        } else if (cellValue.contains("game")) {
                            cellValue = "games";
                        } else if (!cellValue.contains("active") && !cellValue.contains("creative") && !cellValue.contains("media")) {
                            cellValue = "kurio";
                        }
                        type = cellValue;
                        apk.setType(cellValue);
                    } else if (currentColumn == columnWithVersionCode) {
                        apk.setVersionCode(cell.getStringCellValue());
                    } else if (currentColumn == columnWithCross) {
                        String value = "";
                        if (cellValue.toLowerCase().startsWith("x") && cellValue.length() < 20 ||
                                multiLanguage && (cellValue.toLowerCase().contains("fr") || cellValue.toLowerCase().contains("de") || cellValue.toLowerCase().contains("nl") || cellValue.toLowerCase().contains("es"))) {
                            try {
                                boolean cellValueHasCross = cellValue.toLowerCase().startsWith("x");
                                int priorityIndex = 0;
                                if(cellValueHasCross) {
                                    priorityIndex = 1;
                                }
                                if (!cellValue.substring(priorityIndex).isEmpty()) {
                                    String regex = "[0-9]+", priority = "";
                                    value = cellValue.length() > 2 ? cellValue.substring(priorityIndex, 3) : cellValue.substring(priorityIndex, 2);
                                    for (char charValue : value.toCharArray()) {
                                        if (("" + charValue).matches(regex)) {
                                            priority += "" + charValue;
                                        }
                                    }
                                    priority = String.valueOf(100 - Integer.parseInt(priority));
                                    //accord avec Caroline pour que les apps avec priorités soient dans la catégorie Kurio
                                    apk.setType(type);
                                    apk.setPriority(priority);
                                    System.out.println("L'App " + apk.getLabel() + " a une priorité de " + priority);
                                }
                            } catch (NumberFormatException formatException) {
                                //pas de priorite
                            }
                            if (cellValue.toLowerCase().contains("kurio")) {
                                apk.setType("kurio");
                            }
                            if (cellValue.toLowerCase().contains("installed")) {
                                if(!excludeFromAppFolder(apk)) {
                                    downloadFromServer(apk.getFile(), "https://www.kdtablets.com/downloads/apks/", appFolderName, "apk");
                                }
                            }
                            if(cellValue.toLowerCase().contains("fr")) {
                                apk.setFrSelected(true);
                            }
                            if(cellValue.toLowerCase().contains("nl")) {
                                apk.setNlSelected(true);
                            }
                            if(cellValue.toLowerCase().contains("de")) {
                                apk.setDeSelected(true);
                            }
                            if(cellValue.toLowerCase().contains("es")) {
                                apk.setEsSelected(true);
                            }
                            if (cellValue.toLowerCase().contains("x")) {
                                apk.setSelected(true);
                            }
                            if (isMissingData(apk)) {
                                System.out.println("Donnée(s) manquante(s) à la ligne " + (cell.getRowIndex() + 1) + "   -   retiré de l'applist");
                                apk.setSelected(false);
                                apk.setDeSelected(false);
                                apk.setNlSelected(false);
                                apk.setFrSelected(false);
                                apk.setEsSelected(false);
                            }
                        }
                    }
                }
            }
            if (apk.isSelected() || apk.isEsSelected() || apk.isDeSelected() || apk.isNlSelected() || apk.isFrSelected()) {
                boolean isApkDouble = listExtractedApk.stream().anyMatch(c -> c.getPackageName().equals(apk.getPackageName()));
                boolean isEmptyList = listExtractedApk.isEmpty();
                boolean isMissingData = isMissingData(apk);
                if (!isMissingData && (isEmptyList || !isApkDouble)) {
                    listExtractedApk.add(apk);
                }
            }
        }
        return listExtractedApk;
    }

    public static void appListCreator(int versionApplist, String fileToGenerate, ArrayList<Apk> listExtractedApk, String preloadFolderName, String language) throws Exception {
        File applistfile = new File(preloadFolderName + fileToGenerate);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        final String[] attrTable = new String[]{"file", "packageName", "versionCode", "icon", "label", "priority", "type"};

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();

        //applist
        Element root = document.createElement("apks");
        document.appendChild(root);

        Attr attrDate = document.createAttribute("date");
        attrDate.setValue("" + sdf.format(new Date(System.currentTimeMillis())));
        root.setAttributeNode(attrDate);
        Attr attrVer = document.createAttribute("version");
        attrVer.setValue("" + versionApplist);
        root.setAttributeNode(attrVer);

        ArrayList<Apk> listExtractedApkTemp = selectApps(listExtractedApk,language);


        for (Apk apk : listExtractedApkTemp) {
            downloadFromServer(apk.getIcon(), "https://www.kdtablets.com/downloads/appList/icon/", preloadFolderName, "icon");
            if (excludeFromApplist(apk)) {
                //télécharger puis exclure de l'applist twinme et kidoz store apk
                downloadFromServer(apk.getFile(), "https://www.kdtablets.com/downloads/apks/", preloadFolderName, "apk");
                continue;
            }
            Element apkRow = document.createElement("apk");
            root.appendChild(apkRow);

            for (int i = 0; i < attrTable.length; i++) {
                if (attrTable[i].equals("priority") && apk.getPriority() == null) {
                    continue;
                }
                Attr attrApk = document.createAttribute(attrTable[i]);
                switch (attrTable[i]) {
                    case "file":
                        attrApk.setValue(apk.getFile());
                        break;
                    case "packageName":
                        attrApk.setValue(apk.getPackageName());
                        break;
                    case "versionCode":
                        attrApk.setValue(apk.getVersionCode());
                        break;
                    case "icon":
                        attrApk.setValue(apk.getIcon());
                        break;
                    case "label":
                        try {
                            if (language.equals("fr") && !apk.getLabelFR().isEmpty()) {
                                attrApk.setValue(apk.getLabelFR());
                            } else if (language.equals("de") && !apk.getLabelDE().isEmpty()) {
                                attrApk.setValue(apk.getLabelDE());
                            } else if (language.equals("nl") && !apk.getLabelNL().isEmpty()) {
                                attrApk.setValue(apk.getLabelNL());
                            } else if (language.equals("es") && !apk.getLabelES().isEmpty()) {
                                attrApk.setValue(apk.getLabelES());
                            } else {
                                attrApk.setValue(apk.getLabel());
                            }
                        } catch (NullPointerException e) {
                            attrApk.setValue(apk.getLabel());
                        }
                        break;
                    case "priority":
                        attrApk.setValue(apk.getPriority());
                        break;
                    case "type":
                        attrApk.setValue(apk.getType());
                        break;
                }
                apkRow.setAttributeNode(attrApk);
            }

        }

        //appcategory

        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("omit-xml-declaration", "yes");
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(applistfile);
        transformer.transform(source, result);

        // Output to console for testing
        StreamResult consoleResult = new StreamResult(System.out);
        transformer.transform(source, consoleResult);
    }

    static ArrayList<Apk> selectApps(ArrayList<Apk> listExtractedApk, String language) {
        ArrayList<Apk> listExtractedApkTemp = new ArrayList<>();
        listExtractedApkTemp.addAll(listExtractedApk);
        //retirer les apks ne devant pas se trouver dans d'autres langues
        if(multiLanguage) {
            for (Apk apk : listExtractedApk) {
                switch (language) {
                    case "fr":
                        if (!apk.isFrSelected()) {
                            listExtractedApkTemp.remove(apk);
                        }
                        break;
                    case "nl":
                        if (!apk.isNlSelected()) {
                            listExtractedApkTemp.remove(apk);
                        }
                        break;
                    case "de":
                        if (!apk.isDeSelected()) {
                            listExtractedApkTemp.remove(apk);
                        }
                        break;
                    case "es":
                        if (!apk.isEsSelected()) {
                            listExtractedApkTemp.remove(apk);
                        }
                        break;
                    default:
                        if (!apk.isSelected()) {
                            listExtractedApkTemp.remove(apk);
                        }
                        break;
                }
            }
        }
        return listExtractedApkTemp;
    }

    //Apk à exclure de l'applist comme Twinme ou le Kidoz Store par exemples
    static boolean excludeFromApplist(Apk apk) {
        String[] apksPMToExclude = new String[]{"com.kidoz.store.kurio", "org.twinlife.device.android.twinme"};
        for (String apkPackName : apksPMToExclude) {
            if (apk.getPackageName().equals(apkPackName)) {
                return true;
            }
        }
        return false;
    }

    //Apk à exclure du dossier APP comme Twinme, le Kidoz Store, Google Classroom et YT kids par exemples
    static boolean excludeFromAppFolder(Apk apk) {
        String[] apksPMToExclude = new String[]{"com.kidoz.store.kurio", "org.twinlife.device.android.twinme", "com.google.android.apps.classroom", "com.google.android.apps.youtube.kids"};
        for (String apkPackName : apksPMToExclude) {
            if (apk.getPackageName().equals(apkPackName)) {
                return true;
            }
        }
        return false;
    }

    static boolean isMissingData(Apk apk) {
        boolean dataMissing;
        try {
            if (!apk.getIcon().isEmpty() && !apk.getType().isEmpty() && !apk.getFile().isEmpty() && !apk.getLabel().isEmpty() && !apk.getPackageName().isEmpty() && !apk.getVersionCode().isEmpty()) {
                dataMissing = false;
            } else {
                dataMissing = true;
            }
        } catch (NullPointerException exception) {
            dataMissing = true;
        }
        return dataMissing;
    }

    static boolean downloadFromServer(String fileToDownload, String link, String destDir, String type) {
        if (fileToDownload == null) {
            return false;
        } else if (new File(destDir + fileToDownload).exists()) {
            return false;
        }
        //convertir les espaces en %20
        String newurl = (link + fileToDownload).replaceAll(" ", "%20");
        if (isFileOnServer(newurl)) {
            if (type.equals("brand_logo")) fileToDownload = "brand_logo.png";
            try (BufferedInputStream inputStream = new BufferedInputStream(new URL(newurl).openStream());
                 FileOutputStream fileOS = new FileOutputStream(destDir + fileToDownload)) {
                if (type.startsWith("apk")) {
                    System.out.println("Téléchargement de " + fileToDownload);
                }

                byte data[] = new byte[1024];
                int byteContent;
                int fileSize = 0;
                while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
                    fileOS.write(data, 0, byteContent);
                    fileSize += byteContent;
                }
                if((fileToDownload.endsWith(".png") || fileToDownload.endsWith(".icon")) && fileSize > 100000) {
                    errorsToDisplay += "Le fichier image " + link + fileToDownload + " est trop volumineux\n";
                }

            } catch (Exception e) {
                errorsToDisplay += "Le fichier " + link + fileToDownload + " erreur :  " + e.getMessage() + "\n";
                return false;
                //DO-NOTHING
            }
            return true;
        } else if(type.equals("apkSystem")){
            return false;
        } else {
            errorsToDisplay += "Le fichier " + link + fileToDownload + " n'est pas sur le serveur\n";
            return false;
        }
    }

    static boolean isFileOnServer(String URLName) {
        try {
            HttpURLConnection con = (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    static void downloadLastVersion(String apkVersionToDownload, String urlLastKurioVersions, String preloadFolderName) {
        int appversion = 0;
        boolean correctVersionFound = false;
        String fileToDownload = "";
        String versionToDownload = "";
        while (!correctVersionFound && appversion <100) {
            fileToDownload = apkVersionToDownload + appFileTwoFirstIntVersionNumber + ".";
            versionToDownload = (appversion < 10 ? "0" + appversion : "" + appversion);
            correctVersionFound = isFileOnServer((urlLastKurioVersions + fileToDownload + versionToDownload + ".apk").replaceAll(" ", "%20"));
            //Check if higher version available
            if(correctVersionFound && isFileOnServer((urlLastKurioVersions + fileToDownload + ((appversion+1) < 10 ? "0" + (appversion+1) : "" + (appversion+1)) + ".apk").replaceAll(" ", "%20"))) {
                correctVersionFound = false;
            }
            appversion++;
        }
        downloadFromServer(fileToDownload + versionToDownload + ".apk", urlLastKurioVersions, preloadFolderName, "apkSystem");
    }
}
